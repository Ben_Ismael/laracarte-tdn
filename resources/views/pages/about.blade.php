@extends('layouts.default', ["title"=>"About"])

@section('content')
<div class="container">
	<h2> What is {{config('app.name')}} ?</h2>
	<p> {{config('app.name')}} est une copie de <a href="#">Laramap.com</a></p>

	<div class="row">
		<div class="col-md-6">
			<p class="alert alert-warning"> 
			<strong> <i class="fa fa-exclamation-triangle" aria-hidden="true"></i>
				Cette application créer par <a href="#">@benismo</a> à été Créer pour l'apprentissage.
			</strong>	
			</p>
		</div>
	</div>

	<p>Sentez vous libre d'apporter une modification au code <a href="#">Source</a>.</p>

	<hr>

	<h2>What is Laramap</h2>
	<p>Laramap is the website by which {{config('app.name')}} was inspired :).</p>
	<p>More info <a href="#">here</a>.</p>

	<hr>

	<h2>Which tools and services are used in {{config('app.name')}}?</h2>
	<p>Basically it's built on Laravel & Bootstrap. But there's a bunch of services used for email and other sections.</p>

	<ul>
		<li>Laravel</li>
		<li>Bootstrap</li>
		<li>Amazon S3</li>
		<li>Mandrill</li>
		<li>Google map</li>
		<li>Gravatar</li>
		<li>Antony Martin's Geolocation Package</li>
		<li>Michel Fortin's Markdown Parser Package</li>
		<li>Heroku</li>
	</ul>
</div>

@stop
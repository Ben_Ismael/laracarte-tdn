@extends('layouts.default', ["title"=>"Contact"])

@section('content')

<div class="container">
    <div class="col-md-8 col-md-offset-2 col-sm-10 col-sm-offset-1">
    	<h2>Get In Touch</h2>
    	<p class="text-muted"> If you having trouble with this service, please <a href="mailto:ismael.diomande225@gmail.com">ask for help</a>. </p>
    	<form action="" method="post">
    		{{ csrf_field() }}

     <div class="form-group {{ $errors->has('name') ? 'has-error' : '' }}">
    	<label for="name" class="control-label"> Name</label>
    	 <input type="text" id="name" name="name" value="{{ old('name') }}" class="form-control">
   	  {!! $errors->first('name', '<span class="help-block"> :message </span>') !!} 
    </div>

    <div class="form-group {{ $errors->has('email') ? 'has-error' : '' }}">
    	<label for="email" class="control-label"> Email</label>
    	 <input type="text" id="email" name="email" value="{{ old('email') }}" class="form-control">
   	  {!! $errors->first('email', '<span class="help-block"> :message </span>') !!} 
    </div>

    <div class="form-group {{ $errors->has('title') }}">
    	<label for="message" class="control-label sr-only"> Message</label>
    	<textarea id="message" name="message" cols="30" rows="10" class="form-control">{{ old('message')  }}</textarea>
   	  {!! $errors->first('message', '<span class="help-block"> :message </span>') !!}

   </div>

   <div class="form-group">
   	<button class="btn btn-primary btn-block"> Submit Enquiry</button>
   </div>
    	</form>
    </div>
</div>

@stop